#
# Copyright 2014 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Use the non-open-source parts, if they're present
include device/rockchip/common/BoardConfig.mk
TARGET_BOARD_PLATFORM := rk3188
TARGET_BOARD_PLATFORM_GPU := mali400

TARGET_PREBUILT_KERNEL := kernel/arch/arm/boot/Image

TARGET_ARCH := arm
TARGET_ARCH_VARIANT := armv7-a-neon

TARGET_CPU_VARIANT := cortex-a9

TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi
TARGET_CPU_SMP := true
#BOARD_USE_LOW_MEM := true
#BOARD_HAVE_BLUETOOTH := false
PRODUCT_PACKAGE_OVERLAYS += device/rockchip/rk3188/overlay

GRAPHIC_MEMORY_PROVIDER := dma_buf

BOARD_WITH_IOMMU := false

ifeq ($(strip $(BOARD_USE_LOW_MEM)), true)
BUILD_WITH_GOOGLE_MARKET := false
PRODUCT_PROPERTY_OVERRIDES += ro.config.low_ram=true
PRODUCT_PROPERTY_OVERRIDES += dalvik.vm.jit.codecachesize=0
endif

# Copy RK3288 own init.rc file
TARGET_PROVIDES_INIT_RC := true

TARGET_BOARD_PLATFORM_PRODUCT := tablet

#######for target product ########
ifeq ($(strip $(TARGET_BOARD_PLATFORM_PRODUCT)), box)
DEVICE_PACKAGE_OVERLAYS += device/rockchip/common/overlay_screenoff

PRODUCT_PROPERTY_OVERRIDES += \
        ro.target.product=box
else
  PRODUCT_PROPERTY_OVERRIDES += \
        ro.target.product=tablet
endif


PRODUCT_COPY_FILES += \
    device/rockchip/rk3188/fstab.$(TARGET_BOARD_HARDWARE).bootmode.unknown:root/fstab.$(TARGET_BOARD_HARDWARE).bootmode.unknown \
    device/rockchip/rk3188/fstab.$(TARGET_BOARD_HARDWARE).bootmode.emmc:root/fstab.$(TARGET_BOARD_HARDWARE).bootmode.emmc \
    device/rockchip/rk3188/init.$(TARGET_BOARD_HARDWARE).bootmode.emmc.rc:root/init.$(TARGET_BOARD_HARDWARE).bootmode.emmc.rc \
    device/rockchip/rk3188/init.$(TARGET_BOARD_HARDWARE).bootmode.unknown.rc:root/init.$(TARGET_BOARD_HARDWARE).bootmode.unknown.rc

